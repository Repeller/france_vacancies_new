﻿using System.ComponentModel;
using FranceVacancies.Model;

namespace FranceVacancies.ViewModel
{
    internal class ApartmentViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }

        private Apartment.ApartmentType _apartType;
        private string _address;
        private double _size;
        private double _price;
        private int _numberOfRooms;
        private bool _availability;
        private string _description;
        private City.Citys _cityName;
        private string _imagePath;

        public int Id { get; }

        public Apartment.ApartmentType ApartType
        {
            get => _apartType;
            set
            {
                _apartType = value;
                OnPropertyChanged(nameof(_apartType));
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                OnPropertyChanged(nameof(_address));
            }
        }

        public double Size
        {
            get => _size;
            set
            {
                _size = value;
                OnPropertyChanged(nameof(_size));
            }
        }

        public double Price
        {
            get => _price;
            set
            {
                _price = value;
                OnPropertyChanged(nameof(_price));
            }
        }

        public int NumberOfRooms
        {
            get => _numberOfRooms;
            set
            {
                _numberOfRooms = value;
                OnPropertyChanged(nameof(_numberOfRooms));
            }
        }

        public bool Availability
        {
            get => _availability;
            set
            {
                _availability = value;
                OnPropertyChanged(nameof(_availability));
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(_description));
            }
        }

        public City.Citys CityName
        {
            get => _cityName;
            set
            {
                _cityName = value;
                OnPropertyChanged(nameof(_cityName));
            }
        }

        public string ImagePath
        {
            get => _imagePath;
            set
            {
                _imagePath = value;
                OnPropertyChanged(nameof(_imagePath));
            }
        }
    }
}