﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace FranceVacancies.ViewModel
{
	public class ContentViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(name));
		}

		private string _test;

		public string Test
		{
			get => _test;

			set
			{
				_test = value;
				OnPropertyChanged(nameof(Test));
			}
		}

		//public static void HomeOnClick(object sender, RoutedEventArgs e)
		//{
			
		//}

		//public static void ApartmentOnClick(object sender, RoutedEventArgs e)
		//{

		//}

		//public static void ApartmentCatalogOnClick(object sender, RoutedEventArgs e)
		//{

		//}

		//public static void CityOnClick(object sender, RoutedEventArgs e)
		//{

		//}

		//public static void BookningOnClick(object sender, RoutedEventArgs e)
		//{

		//}
	}
}