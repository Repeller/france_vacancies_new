﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class DisplayBookning
	{
		public DateTime Start { get; }
		public DateTime End { get;  }
		public int ApartId { get; }

		public DisplayBookning(DateTime start, DateTime end, int apartId)
		{
			Start = start;
			End = end;
			ApartId = apartId;
		}

	}
}
