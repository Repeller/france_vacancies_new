﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	internal class ApartmentCatalog
	{
		public List<Apartment> Apartments { get; set; }

		//public DisplayBookDist ApartBooknings { get; set; }


		public ApartmentCatalog()
		{
			Apartments = new List<Apartment>();
		}

		public void AddOneApartment(Apartment tempApartment)
		{
			Apartments.Add(tempApartment);
		}

		public void PrintAll()
		{
			//foreach (var tempApartment in Apartments)
			//{
			//	Console.WriteLine(
			//		$"id '{tempApartment.Id}' (available : {tempApartment.Available} ) - type {tempApartment.Type} , address '{tempApartment.Address}'");
			//	Console.WriteLine(
			//		$"number of rooms '{tempApartment.NumberOfRooms}' size '{tempApartment.Size}' - price {tempApartment.Price}");
			//	Console.WriteLine($"Description: {tempApartment.Description}");
			//	Console.WriteLine("< ---------- ------------ >");
			//}
		}

		public void LoadApartments()
		{
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Ember Way 25", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ",
				City.Citys.Paris));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Berry Lane 534", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Lille));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Law Boulevard 123", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Bordeaux));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Heritage Street 21-C", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Nantes));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Wetland Street 231", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Nantes));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Star Lane 1", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Paris));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Beachside Passage 43", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Strasbourg));
			Apartments.Add(new Apartment(Apartment.ApartmentType.house, "Luna Passage 213", 50.5, 50.5, 200.5,
				"Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. ", 
				City.Citys.Montpellier));
		}
	}
}
