﻿namespace FranceVacancies.Model
{
	internal class City
	{
		public enum Citys
		{
			Paris,
			Marseille,
			Lyon,
			Toulouse,
			Nice,
			Nantes,
			Strasbourg,
			Montpellier,
			Lille,
			Bordeaux
		}

		public City(int zipcode, Citys cityName, string description, string imagePath)
		{
			ZipCode = zipcode;
			CityName = cityName;
			Description = description;
			ImagePath = imagePath;
		}

		public int ZipCode { get; set; }
		public Citys CityName { get; set; }
		public string Description { get; set; }
		public string ImagePath { get; set; }
	}
}