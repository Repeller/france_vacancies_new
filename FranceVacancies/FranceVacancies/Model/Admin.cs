﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class Admin : BasicUser
	{
		public static int CountOfAdmins = 0;

		public int AdminId { get; } // added

		public Admin(string userName, string password, string name, string email) : base(name, email, userName, password)
		{
			AdminId = 1 + CountOfAdmins;

			CountOfAdmins++;
		}
	}
}
