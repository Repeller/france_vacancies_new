﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class Bookning
	{
		public static int NumberOfOrders;

		public int OrderNumber { get; }
		public int ApartmentId { get; set; }
		public int UserId { get; set; }
		public DateTime StartDateTime { get; set; }
		public DateTime EndDataTime { get; set; }

		public Bookning(int apartmentId, int userId, DateTime startDataTime, DateTime endDataTime)
		{
			OrderNumber = 1 + NumberOfOrders;

			ApartmentId = apartmentId;
			UserId = userId;
			StartDateTime = startDataTime;
			EndDataTime = endDataTime;

			NumberOfOrders++;
		}
	}
}
