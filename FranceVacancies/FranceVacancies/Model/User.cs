﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class User : BasicUser
	{
		public string Address { get; set; }
		public string PhoneNumber { get; set; }
		public string CardInfo { get; set; }

		public User(string name, string address, string email, string phoneNumber, string cardInfo, string username, string password) : base(name, email, username, password)
		{
			Address = address;

			PhoneNumber = phoneNumber;
			CardInfo = cardInfo;
		}
	}
}
