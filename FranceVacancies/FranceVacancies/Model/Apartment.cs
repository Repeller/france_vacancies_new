﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class Apartment
	{
		public static int CountOfApartments;

		public enum ApartmentType
		{
			cottage,
			apartment,
			house,
			villa,
			other
		}

		public int Id { get; }
		public ApartmentType Type { get; set; }
		public string Address { get; set; }
		public bool Available { get; set; }
		public double Size { get; set; }
		public double NumberOfRooms { get; set; }
		public double Price { get; set; }
		public string Description { get; set; }
		public City.Citys CityName { get; set; }

		public Apartment(ApartmentType type, string address, double size, double numberOfRooms, double price,
			string description, City.Citys cityName)
		{
			Id = 1 + CountOfApartments;

			Type = type;
			Address = address;
			Available = true;
			Size = size;
			NumberOfRooms = numberOfRooms;
			Price = price;
			Description = description;
			CityName = cityName;

			CountOfApartments++;
		}
	}
}
