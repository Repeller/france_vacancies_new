﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class DisplayBookDist
	{
		public Dictionary<int, DisplayBookning> Booknings { get; set;  }

		public DisplayBookDist()
		{
			Booknings = new Dictionary<int, DisplayBookning>();
		}

		public DisplayBookDist(Dictionary<int, DisplayBookning> booknings)
		{
			Booknings = booknings;
		}
	}
}
