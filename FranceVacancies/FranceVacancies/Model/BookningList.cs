﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FranceVacancies.Model
{
	class BookningList
	{
		// static public List<Bookning> Booknings { get; set; }
		public static Dictionary<int, Bookning> Booknings {get; set; }

		public BookningList()
		{
			Booknings = new Dictionary<int, Bookning>();
		}

		public void AddOneBookning(Bookning tempBookning)
		{
			Booknings.Add(tempBookning.OrderNumber, tempBookning);
		}
		public void AddOneBookning(int apartmentId, int userId, DateTime startDateTime, DateTime endDataTime)
		{
			Bookning tempBookning = new Bookning(apartmentId, userId, startDateTime, endDataTime);
			Booknings.Add(tempBookning.ApartmentId, tempBookning);
		}

		private bool CheckDateBefore(DateTime userStart, DateTime bookningEnd)
		{
			if (userStart > bookningEnd && userStart > bookningEnd)
				return true;
			else
			{
				return false;
			}
		}
		private bool CheckDateDoing(DateTime userStart, DateTime userEnd, DateTime bookningStart, DateTime bookningEnd)
		{
			if (userStart < bookningStart && userEnd > bookningEnd)
				return true;
			else
			{
				return false;
			}
		}

		private bool CheckDateAfter(DateTime userEnd, DateTime bookningEnd)
		{
			if (userEnd < bookningEnd && userEnd < bookningEnd)
				return true;
			else
			{
				return false;
			}
		}

		private bool CanIBook(bool before, bool doing, bool after)
		{
			// yes

			// T F F
			if (before == true && doing == false && after == false)
				return true;
			// F F T
			if (before == false && doing == false && after == true)
				return true;
			// F F F
			if (before == false && doing == false && after == false)
				return true;
			// T F T
			if (before == true && doing == false && after == true)
				return true;
			// no
			// F T F
			if (before == false && doing == true && after == false)
				return false;
			// T T F
			if (before == true && doing == true && after == false)
				return false;
			// F T T
			if (before == false && doing == true && after == true)
				return false;
			// T T T 
			if (before == true && doing == true && after == true)
				return false;

			
			return false;
		}

		//public List<int> CheckAvailable(DateTime start, DateTime end)
		//{
		//	List<int> apartmentIds;
		//	List<int> apartmentToChangeId;
		
		//	return apartmentIds;
		//}


		public void PrintOutAll()
		{

		}
	}
}
